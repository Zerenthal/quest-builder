package net.bobmandude9889.questBuilder.quest;

import java.util.HashMap;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class TriggerData {
	
	public List<TriggerType> triggers;
	public List<HashMap<String,String>> params;

	public TriggerData(List<TriggerType> triggers, List<HashMap<String,String>> params) {
		this.triggers = triggers;
		this.params = params;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSON() {
		JSONArray arr = new JSONArray();
		for (int i = 0; i < triggers.size(); i++) {
			TriggerType trigger = triggers.get(i);
			JSONObject obj = new JSONObject();
			obj.put("action", trigger.name());
			for (String key : params.get(i).keySet()) {
				obj.put(key, params.get(i).get(key));
			}
			arr.add(obj);
		}
		return arr.toJSONString();
	}

}
