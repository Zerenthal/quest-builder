package net.bobmandude9889.questBuilder.quest;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import net.bobmandude9889.questBuilder.QuestBuilder;
import net.bobmandude9889.questBuilder.dialogue.DialogueAction;
import net.bobmandude9889.questBuilder.util.sql.DBConn;
import net.bobmandude9889.questBuilder.util.sql.SqlStatement;

public class Quest {

	static List<Quest> quests;

	public TriggerData trigger;
	
	public int id;
	public String name;
	public String description;
	public String startDescription;
	public List<Goal> goals;

	public DialogueAction startAction;
	public DialogueAction endAction;

	static {
		try {
			quests = new ArrayList<>();

			DBConn conn = QuestBuilder.sqlMCConn;
			SqlStatement stmt = new SqlStatement(conn);

			ResultSet results = stmt.select("Quests", "1 ORDER BY Id");
			while (results.next()) {
				ResultSet goalResults = stmt.select("Goals", "ParentQuest=" + results.getInt("Id") + " ORDER BY Id");
				List<Goal> goals = new ArrayList<>();

				String startTriggerString = results.getString("StartTrigger");
				TriggerData trigger = getTriggerData(startTriggerString);

				DialogueAction startAction = loadActionFromString(results.getString("StartAction"));
				DialogueAction endAction = loadActionFromString(results.getString("EndAction"));

				Quest quest = new Quest(results.getInt("Id"), results.getString("Name"), results.getString("Description"), results.getString("StartDescription"), goals, trigger, startAction, endAction);
				while (goalResults.next()) {
					String triggerString = goalResults.getString("EndTrigger");
					TriggerData type = getTriggerData(triggerString);

					DialogueAction start = loadActionFromString(goalResults.getString("StartAction"));
					DialogueAction complete = loadActionFromString(goalResults.getString("EndAction"));

					quest.goals.add(new Goal(goalResults.getInt("Id"), type, goalResults.getString("Name"), goalResults.getString("Description"), quest, start, complete));
				}
				quests.add(quest);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static TriggerData getTriggerData(String trigger) {
		JSONArray jsonObj = null;
		try {
			jsonObj = (JSONArray) new JSONParser().parse(trigger);
			List<TriggerType> types = new ArrayList<>();
			List<HashMap<String,String>> paramMap = new ArrayList<>();
			for (int i = 0; i < jsonObj.size(); i++) {
				JSONObject triggerObj = (JSONObject) jsonObj.get(i);
				types.add(TriggerType.valueOf(triggerObj.get("action").toString()));
				HashMap<String,String> params = new HashMap<>();
				for(Object key : triggerObj.keySet()) {
					if(!key.equals("action"))
						params.put((String) key, (String) triggerObj.get(key));
				}
				paramMap.add(params);
			}
			System.out.println(types + " " + paramMap);
			TriggerData data = new TriggerData(types, paramMap);
			return data;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static DialogueAction loadActionFromString(String raw) {
		if (raw == null)
			return null;

		DialogueAction action = new DialogueAction();
		JSONObject jsonObj;
		try {
			jsonObj = (JSONObject) new JSONParser().parse(raw);
			for (Object key : jsonObj.keySet()) {
				if (action.getClass().getDeclaredField(key.toString()).getType().equals(int.class))
					action.set(key.toString(), ((Long) (jsonObj.get(key))).intValue());
				else
					action.set(key.toString(), jsonObj.get(key));
			}
		} catch (ParseException | NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		}

		return action;
	}

	public static List<Quest> getQuests() {
		return quests;
	}

	public static Quest getQuest(int id) {
		for (Quest quest : quests) {
			if (quest.id == id)
				return quest;
		}
		return null;
	}
	
	public static Quest createQuest() {
		DBConn conn = QuestBuilder.sqlMCConn;
		SqlStatement stmt = new SqlStatement(conn);
		stmt.execute("INSERT INTO `Quests` (`Name`, `Description`, `StartDescription`, `Id`, `StartTrigger`, `StartAction`, `EndAction`) VALUES (NULL, NULL, NULL, NULL, '[]', '{}', '{}');");
		ResultSet result = stmt.query("SELECT Id FROM Quests ORDER BY Id DESC;");
		Quest quest = null;
		try {
			result.next();
			quest = new Quest(result.getInt("Id"), "", "", "", new ArrayList<>(), new TriggerData(new ArrayList<>(), new ArrayList<>()), new DialogueAction(), new DialogueAction());
			quests.add(quest);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return quest;
	}

	public Quest(int id, String name, String description, String startDescription, List<Goal> goals, TriggerData trigger, DialogueAction startAction, DialogueAction endAction) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.startDescription = startDescription;
		this.goals = goals;
		this.startAction = startAction;
		this.endAction = endAction;
		this.trigger = trigger;
	}

	public Goal getGoal(String name) {
		for (Goal goal : goals) {
			if (goal.name.equals(name))
				return goal;
		}
		return null;
	}

	public Goal getGoal(int i) {
		if(this.goals.size() > i && i >= 0) {
			return goals.get(i);
		}
		return null;
	}

	public boolean hasNextGoal(Goal current) {
		int index = goals.indexOf(current);
		return (index < goals.size() - 1);
	}

	public int getNextGoal(Goal current) {
		int index = goals.indexOf(current);
		if (index < goals.size() - 1) {
			return index + 1;
		}
		return -1;
	}
	
	public void delete() {
		DBConn conn = QuestBuilder.sqlMCConn;
		SqlStatement stmt = new SqlStatement(conn);
		
		stmt.execute("DELETE FROM `Quests` WHERE `Id` = " + this.id);
		
		Quest.quests.remove(this);
		for (Goal goal : goals) {
			goal.delete();
		}
	}
	
	public void update() {
		DBConn conn = QuestBuilder.sqlMCConn;
		SqlStatement stmt = new SqlStatement(conn);
		
		System.out.println("update quest");
		
		stmt.prepExe("UPDATE `Quests` SET "
				+ "`Name`=?,"
				+ "`Description`=?,"
				+ "`StartDescription`=?,"
				+ "`StartTrigger`=?,"
				+ "`StartAction`=?,"
				+ "`EndAction`=? WHERE `Id` = " + this.id, "ssssss", this.name, this.description, this.startDescription, this.trigger.toJSON(), this.startAction.toJSON(), this.endAction.toJSON());
		
		for (Goal goal : goals) {
			goal.update();
		}
	}

}
