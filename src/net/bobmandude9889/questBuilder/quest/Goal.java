package net.bobmandude9889.questBuilder.quest;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import net.bobmandude9889.questBuilder.QuestBuilder;
import net.bobmandude9889.questBuilder.dialogue.DialogueAction;
import net.bobmandude9889.questBuilder.util.sql.DBConn;
import net.bobmandude9889.questBuilder.util.sql.SqlStatement;

public class Goal {

	Quest parent;
	
	public TriggerData trigger;
	
	public int id;
	public String desc;
	public String name;

	public DialogueAction startAction;
	public DialogueAction endAction;
	
	public Goal(int id, TriggerData trigger, String name, String desc, Quest parent, DialogueAction start, DialogueAction complete) {
		this.trigger = trigger;
		this.name = name;
		this.desc = desc;
		this.parent = parent;
		this.startAction = start;
		this.endAction = complete;
		this.id = id;
	}
	
	public static Goal createGoal(Quest parent) {
		DBConn conn = QuestBuilder.sqlMCConn;
		SqlStatement stmt = new SqlStatement(conn);
		stmt.execute("INSERT INTO `Goals` (`Name`, `Description`, `ParentQuest`, `Id`, `EndTrigger`, `StartAction`, `EndAction`) VALUES (NULL, NULL, " + parent.id + ", null, '[]', '{}', '{}');");
		ResultSet result = stmt.query("SELECT Id FROM Goals ORDER BY Id DESC;");
		Goal goal = null;
		try {
			result.next();
			goal = new Goal(result.getInt("Id"), new TriggerData(new ArrayList<>(), new ArrayList<>()), "", "", parent, new DialogueAction(), new DialogueAction());
			parent.goals.add(goal);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return goal;
	}
	
	public void delete() {
		DBConn conn = QuestBuilder.sqlMCConn;
		SqlStatement stmt = new SqlStatement(conn);
		
		stmt.execute("DELETE FROM `Goals` WHERE `Id` = " + this.id);
		parent.goals.remove(this);
	}
	
	public void update() {
		DBConn conn = QuestBuilder.sqlMCConn;
		SqlStatement stmt = new SqlStatement(conn);
		
		stmt.prepExe("UPDATE `Goals` SET "
				+ "`Name`=?,"
				+ "`Description`=?,"
				+ "`EndTrigger`=?,"
				+ "`StartAction`=?,"
				+ "`EndAction`=? WHERE `Id` = " + this.id, "sssss", this.name, this.desc, this.trigger.toJSON(), this.startAction.toJSON(), this.endAction.toJSON());
	}
	
	public String toString() {
		return this.name + " (" + this.id + ")";
	};
	
}