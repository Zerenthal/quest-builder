package net.bobmandude9889.questBuilder.quest;

public enum TriggerType {
	//enterRegion <name>
	enterRegion("Enter Region"),
	//leaveRegion <name>
	leaveRegion("Leave Region"),
	//gainFlag <name>
	gainFlag("Gain Flag"),
	//loseFlag <name>
	loseFlag("Lose Flag"),
	//receiveItem <id> [data] [name]
	receiveItem("Receive Item"),
	//sellItem <id> [data] [name]
	sellItem("Sell Item"),
	//buyItem <id> [data] [name]
	buyItem("Buy Item"),
	//reachLevel <level> <skill>
	reachLevel("Reach Level"),
	//reachXp <xp> <skill>
	reachXp("Reach Xp"),
	//giveNPCItem <id> [data] [name]
	giveNPCItem("Give NPC Item");
	
	private String display;
	
	private TriggerType(String display) {
		this.display = display;
	}
	
	public String display() {
		return display;
	}
	
}
