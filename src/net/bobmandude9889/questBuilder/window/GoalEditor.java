package net.bobmandude9889.questBuilder.window;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import org.bukkit.Material;

import net.bobmandude9889.questBuilder.npc.NPCType;
import net.bobmandude9889.questBuilder.quest.Goal;
import net.bobmandude9889.questBuilder.quest.TriggerType;
import net.bobmandude9889.questBuilder.util.Util;
import net.bobmandude9889.questBuilder.window.callback.GoalEditCallback;
import net.bobmandude9889.questBuilder.window.callback.TriggerEditCallback;

public class GoalEditor extends JFrame {

	private static final long serialVersionUID = -6871612589257314203L;
	private JPanel contentPane;

	private JTextField txtName;
	
	private JButton saveBtn;
	
	public GoalEditor(Goal goal, GoalEditCallback callback) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setResizable(false);
		setBounds(100, 100, 761, 358);
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setTitle("Editing goal #" + goal.id);
		
		JLabel lblName = new JLabel("Name:");
		lblName.setBounds(10, 11, 100, 14);
		getContentPane().add(lblName);

		txtName = new JTextField();
		txtName.setText(goal.name);
		txtName.setBounds(10, 36, 100, 20);
		getContentPane().add(txtName);
		txtName.setColumns(10);

		JLabel lblDescription = new JLabel("Description:");
		lblDescription.setBounds(10, 67, 100, 14);
		getContentPane().add(lblDescription);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 92, 100, 80);
		getContentPane().add(scrollPane);
		
		JTextPane textPane_1 = new JTextPane();
		scrollPane.setViewportView(textPane_1);
		textPane_1.setText(goal.desc);

		saveBtn = new JButton("Save");
		saveBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				goal.name = txtName.getText();
				goal.desc = textPane_1.getText();
				goal.update();
				
				QuestSelection.instance.updateModel();
				
				GoalEditor.this.setVisible(false);
				GoalEditor.this.dispose();
				
				callback.onSave(goal);
			}
		});
		saveBtn.setBounds(645, 296, 100, 23);
		getContentPane().add(saveBtn);
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.setBounds(645, 262, 100, 23);
		btnDelete.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				goal.delete();
				callback.onDelete();
			}
		});
		contentPane.add(btnDelete);
		
		JLabel lblStartAction = new JLabel("Start Action:");
		lblStartAction.setBounds(146, 11, 150, 14);
		getContentPane().add(lblStartAction);

		Util.setupAction(goal.startAction, 0, contentPane, saveBtn);

		JLabel lblEndAction = new JLabel("End Action:");
		lblEndAction.setBounds(398, 11, 150, 14);
		getContentPane().add(lblEndAction);
		
		Util.setupAction(goal.endAction, 252, contentPane, saveBtn);
		
		JLabel lblStartTrigger = new JLabel("Start Trigger:");
		lblStartTrigger.setBounds(645, 11, 100, 14);
		getContentPane().add(lblStartTrigger);

		JComboBox<String> comboBox = new JComboBox<String>();
		String defaultSelected = goal.trigger.triggers.size() > 0 ? goal.trigger.triggers.get(0).name() : null;
		final List<ListDataListener> listeners = new ArrayList<>();
		comboBox.setModel(new ComboBoxModel<String>() {
			
			String selected = defaultSelected;
			
			@Override
			public void removeListDataListener(ListDataListener l) {
				listeners.remove(l);
			}
			
			@Override
			public int getSize() {
				return goal.trigger.triggers.size();
			}
			
			@Override
			public String getElementAt(int index) {
				int count = 0;
				for (int i = 0; i < index; i++) {
					if(goal.trigger.triggers.get(i).equals(goal.trigger.triggers.get(index)))
						count++;
				}
				return goal.trigger.triggers.get(index).name() + (count > 0 ? "(" + count + ")": "");
			}
			
			@Override
			public void addListDataListener(ListDataListener l) {
				listeners.add(l);
			}
			
			@Override
			public void setSelectedItem(Object trigger) {
				this.selected = (String) trigger;
			}
			
			@Override
			public Object getSelectedItem() {
				return selected;
			}
		});
		comboBox.setBounds(645, 36, 100, 20);
		getContentPane().add(comboBox);
		
		JButton btnEdit = new JButton("Edit");

		JButton btnAdd = new JButton("Add");
		btnAdd.setBounds(645, 92, 100, 23);
		getContentPane().add(btnAdd);
		btnAdd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				TriggerType type = TriggerType.values()[0];
				goal.trigger.triggers.add(type);
				HashMap<String, String> map = new HashMap<>();
				for (String key : TriggerEditor.triggers.get(type).keySet()) {
					String name = key.endsWith("*") ? key.substring(0,key.length() - 1) : key;
					String triggerType = TriggerEditor.triggers.get(type).get(key);
					if (triggerType.equals("String")) {
						map.put(name, "");
					} else if (triggerType.equals("Material")) {
						map.put(name, Material.ACACIA_DOOR.name());
					} else if (triggerType.equals("int")) {
						map.put(name, "1");
					} else if (triggerType.equals("NPC")) {
						map.put(name, NPCType.get(0).getName());
					}
				}
				goal.trigger.params.add(map);
				for (ListDataListener l : listeners) {
					l.contentsChanged(new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, 0, 1));
				}
				comboBox.getModel().setSelectedItem(comboBox.getModel().getElementAt(goal.trigger.triggers.size() - 1));
				comboBox.repaint();
				btnEdit.doClick();
			}
		});
		
		btnEdit.setBounds(645, 63, 100, 23);
		getContentPane().add(btnEdit);
		btnEdit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				final int i = comboBox.getSelectedIndex();
				TriggerEditor tEditor = new TriggerEditor(goal.trigger, i, new TriggerEditCallback() {
					
					@Override
					public void onSave(TriggerType type, HashMap<String, String> params, int index) {
						goal.trigger.triggers.set(index, type);
						goal.trigger.params.set(index, params);
						comboBox.getModel().setSelectedItem(comboBox.getModel().getElementAt(index));
						comboBox.repaint();
					}
					
					@Override
					public void onDelete(int index) {
						goal.trigger.triggers.remove(index);
						goal.trigger.params.remove(index);
						comboBox.getModel().setSelectedItem(comboBox.getModel().getElementAt(0));
						comboBox.repaint();
					}
				});
				tEditor.setVisible(true);
			}
			
		});
	}
	
}
