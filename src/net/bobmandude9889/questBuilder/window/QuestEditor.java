package net.bobmandude9889.questBuilder.window;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import org.bukkit.Material;

import net.bobmandude9889.questBuilder.npc.NPCType;
import net.bobmandude9889.questBuilder.quest.Goal;
import net.bobmandude9889.questBuilder.quest.Quest;
import net.bobmandude9889.questBuilder.quest.TriggerType;
import net.bobmandude9889.questBuilder.util.Util;
import net.bobmandude9889.questBuilder.window.callback.GoalEditCallback;
import net.bobmandude9889.questBuilder.window.callback.TriggerEditCallback;

public class QuestEditor extends JFrame {

	private static final long serialVersionUID = -1406523460889539894L;

	Quest quest;
	private JTextField txtTest;
	
	private JButton saveBtn;

	public QuestEditor(Quest quest) {
		this.quest = quest;
		setResizable(false);
		setBounds(100, 100, 761, 358);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		getContentPane().setLayout(null);
		setTitle("Editing quest #" + quest.id);

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}

		addWindowListener(new WindowListener() {

			@Override
			public void windowOpened(WindowEvent e) {
			}

			@Override
			public void windowClosing(WindowEvent e) {
				saveBtn.doClick();
			}

			@Override
			public void windowClosed(WindowEvent e) {
			}

			@Override
			public void windowIconified(WindowEvent e) {
			}

			@Override
			public void windowDeiconified(WindowEvent e) {
			}

			@Override
			public void windowActivated(WindowEvent e) {
			}

			@Override
			public void windowDeactivated(WindowEvent e) {
			}
			
		});

		saveBtn = new JButton("Save");
		
		JLabel lblName = new JLabel("Name:");
		lblName.setBounds(10, 11, 100, 14);
		getContentPane().add(lblName);

		txtTest = new JTextField();
		txtTest.setText(quest.name);
		txtTest.setBounds(10, 36, 100, 20);
		getContentPane().add(txtTest);
		txtTest.setColumns(10);

		JLabel lblDescription = new JLabel("Description:");
		lblDescription.setBounds(10, 67, 100, 14);
		getContentPane().add(lblDescription);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 92, 100, 80);
		getContentPane().add(scrollPane);
		
		JTextPane textPane_1 = new JTextPane();
		scrollPane.setViewportView(textPane_1);
		textPane_1.setText(quest.description);

		JLabel lblStartDescription = new JLabel("Start Description:");
		lblStartDescription.setBounds(10, 183, 100, 14);
		getContentPane().add(lblStartDescription);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 208, 100, 80);
		getContentPane().add(scrollPane_1);

		JTextPane textPane = new JTextPane();
		scrollPane_1.setViewportView(textPane);
		textPane.setText(quest.startDescription);

		JLabel lblStartTrigger = new JLabel("Start Trigger:");
		lblStartTrigger.setBounds(645, 11, 100, 14);
		getContentPane().add(lblStartTrigger);

		JComboBox<String> comboBox = new JComboBox<String>();
		String defaultSelected = quest.trigger.triggers.size() > 0 ? quest.trigger.triggers.get(0).name() : null;
		final List<ListDataListener> listeners = new ArrayList<>();
		comboBox.setModel(new ComboBoxModel<String>() {
			
			String selected = defaultSelected;
			
			@Override
			public void removeListDataListener(ListDataListener l) {
				listeners.remove(l);
			}
			
			@Override
			public int getSize() {
				return quest.trigger.triggers.size();
			}
			
			@Override
			public String getElementAt(int index) {
				int count = 0;
				for (int i = 0; i < index; i++) {
					if(quest.trigger.triggers.get(i).equals(quest.trigger.triggers.get(index)))
						count++;
				}
				return quest.trigger.triggers.get(index).name() + (count > 0 ? "(" + count + ")": "");
			}
			
			@Override
			public void addListDataListener(ListDataListener l) {
				listeners.add(l);
			}
			
			@Override
			public void setSelectedItem(Object trigger) {
				this.selected = (String) trigger;
			}
			
			@Override
			public Object getSelectedItem() {
				return selected;
			}
		});
		comboBox.setBounds(645, 36, 100, 20);
		getContentPane().add(comboBox);
		
		JButton btnEdit = new JButton("Edit");

		JButton btnAdd = new JButton("Add");
		btnAdd.setBounds(645, 92, 100, 23);
		getContentPane().add(btnAdd);
		btnAdd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				TriggerType type = TriggerType.values()[0];
				quest.trigger.triggers.add(type);
				HashMap<String, String> map = new HashMap<>();
				for (String key : TriggerEditor.triggers.get(type).keySet()) {
					String name = key.endsWith("*") ? key.substring(0,key.length() - 1) : key;
					String triggerType = TriggerEditor.triggers.get(type).get(key);
					if (triggerType.equals("String")) {
						map.put(name, "");
					} else if (triggerType.equals("Material")) {
						map.put(name, Material.ACACIA_DOOR.name());
					} else if (triggerType.equals("int")) {
						map.put(name, "1");
					} else if (triggerType.equals("NPC")) {
						map.put(name, NPCType.get(0).getName());
					}
				}
				quest.trigger.params.add(map);
				for (ListDataListener l : listeners) {
					l.contentsChanged(new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, 0, 1));
				}
				comboBox.getModel().setSelectedItem(comboBox.getModel().getElementAt(quest.trigger.triggers.size() - 1));
				comboBox.repaint();
				btnEdit.doClick();
			}
		});
		
		btnEdit.setBounds(645, 63, 100, 23);
		getContentPane().add(btnEdit);
		btnEdit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				final int i = comboBox.getSelectedIndex();
				TriggerEditor tEditor = new TriggerEditor(quest.trigger, i, new TriggerEditCallback() {
					
					@Override
					public void onSave(TriggerType type, HashMap<String, String> params, int index) {
						quest.trigger.triggers.set(index, type);
						quest.trigger.params.set(index, params);
						comboBox.getModel().setSelectedItem(comboBox.getModel().getElementAt(index));
						comboBox.repaint();
					}
					
					@Override
					public void onDelete(int index) {
						quest.trigger.triggers.remove(index);
						quest.trigger.params.remove(index);
						if (quest.trigger.triggers.size() > 0)
							comboBox.getModel().setSelectedItem(comboBox.getModel().getElementAt(0));
						else 
							comboBox.getModel().setSelectedItem(null);
						comboBox.repaint();
					}
				});
				tEditor.setVisible(true);
			}
			
		});

		JLabel lblStartAction = new JLabel("Start Action:");
		lblStartAction.setBounds(146, 11, 150, 14);
		getContentPane().add(lblStartAction);

		Util.setupAction(quest.startAction, 0, getContentPane(), saveBtn);

		JLabel lblEndAction = new JLabel("End Action:");
		lblEndAction.setBounds(398, 11, 150, 14);
		getContentPane().add(lblEndAction);
		
		Util.setupAction(quest.endAction, 252, getContentPane(), saveBtn);
		
		saveBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				quest.name = txtTest.getText();
				quest.description = textPane_1.getText();
				quest.startDescription = textPane.getText();
				quest.update();
				
				QuestSelection.instance.updateModel();
				
				QuestEditor.this.setVisible(false);
				QuestEditor.this.dispose();
			}
		});
		saveBtn.setBounds(645, 296, 100, 23);
		getContentPane().add(saveBtn);
		
		JLabel lblGoalgs = new JLabel("Goals:");
		lblGoalgs.setBounds(645, 126, 100, 14);
		getContentPane().add(lblGoalgs);
		
		JComboBox<Goal> comboBoxGoals = new JComboBox<Goal>();
		comboBoxGoals.setBounds(645, 151, 100, 20);
		getContentPane().add(comboBoxGoals);
		Goal defaultGoal = quest.goals.size() > 0 ? quest.goals.get(0) : null;
		List<ListDataListener> gListeners = new ArrayList<>();
		comboBoxGoals.setModel(new ComboBoxModel<Goal>() {

			Goal selected = defaultGoal;
			
			@Override
			public int getSize() {
				return quest.goals.size();
			}

			@Override
			public Goal getElementAt(int index) {
				return quest.goals.get(index);
			}

			@Override
			public void addListDataListener(ListDataListener l) {
				gListeners.add(l);
			}

			@Override
			public void removeListDataListener(ListDataListener l) {
				gListeners.remove(l);
			}

			@Override
			public void setSelectedItem(Object selected) {
				this.selected = (Goal) selected;
			}

			@Override
			public Object getSelectedItem() {
				return selected;
			}
			
		});
		
		JButton btnEditGoals = new JButton("Edit");
		btnEditGoals.setBounds(645, 179, 100, 23);
		getContentPane().add(btnEditGoals);
		btnEditGoals.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				GoalEditor gEditor = new GoalEditor((Goal) comboBoxGoals.getSelectedItem(), new GoalEditCallback() {
					
					@Override
					public void onSave(Goal goal) {
						for (ListDataListener l : gListeners) {
							l.contentsChanged(new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, 0, 1));
						}
					}
					
					@Override
					public void onDelete() {
						for (ListDataListener l : gListeners) {
							l.contentsChanged(new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, 0, 1));
						}
					}
				});
				gEditor.setVisible(true);
			}
		});
		
		JButton btnAddGoals = new JButton("Add");
		btnAddGoals.setBounds(645, 208, 100, 23);
		getContentPane().add(btnAddGoals);
		btnAddGoals.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Goal goal = Goal.createGoal(quest);
				GoalEditor gEditor = new GoalEditor(goal, new GoalEditCallback() {
					
					@Override
					public void onSave(Goal goal) {
						for (ListDataListener l : gListeners) {
							l.contentsChanged(new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, 0, 1));
						}
					}
					
					@Override
					public void onDelete() {
						for (ListDataListener l : gListeners) {
							l.contentsChanged(new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, 0, 1));
						}
					}
				});
				gEditor.setVisible(true);
				for (ListDataListener l : gListeners) {
					l.contentsChanged(new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, 0, 1));
				}
			}
		});
	}
	
	public static QuestEditor openFor(Quest quest) {
		QuestEditor editor = new QuestEditor(quest);
		editor.setVisible(true);
		return editor;
	}

	public void hide() {
		this.setVisible(true);
	}
}
