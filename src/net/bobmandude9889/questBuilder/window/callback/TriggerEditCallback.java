package net.bobmandude9889.questBuilder.window.callback;

import java.util.HashMap;

import net.bobmandude9889.questBuilder.quest.TriggerType;

public interface TriggerEditCallback {

	public void onSave(TriggerType type, HashMap<String, String> params, int index);
	public void onDelete(int index);
	
}
