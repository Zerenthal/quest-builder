package net.bobmandude9889.questBuilder.window.callback;

public interface CommandEditCallback {

	public void onSave(String command);
	public void onDelete();
	
}
