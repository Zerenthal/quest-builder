package net.bobmandude9889.questBuilder.window.callback;

import net.bobmandude9889.questBuilder.quest.Goal;

public interface GoalEditCallback {
	
	public void onSave(Goal goal);
	public void onDelete();
	
}
