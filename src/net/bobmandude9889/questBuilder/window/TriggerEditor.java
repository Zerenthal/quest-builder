package net.bobmandude9889.questBuilder.window;

import static net.bobmandude9889.questBuilder.quest.TriggerType.buyItem;
import static net.bobmandude9889.questBuilder.quest.TriggerType.enterRegion;
import static net.bobmandude9889.questBuilder.quest.TriggerType.gainFlag;
import static net.bobmandude9889.questBuilder.quest.TriggerType.giveNPCItem;
import static net.bobmandude9889.questBuilder.quest.TriggerType.leaveRegion;
import static net.bobmandude9889.questBuilder.quest.TriggerType.loseFlag;
import static net.bobmandude9889.questBuilder.quest.TriggerType.reachLevel;
import static net.bobmandude9889.questBuilder.quest.TriggerType.reachXp;
import static net.bobmandude9889.questBuilder.quest.TriggerType.receiveItem;
import static net.bobmandude9889.questBuilder.quest.TriggerType.sellItem;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListDataListener;

import org.bukkit.Material;

import net.bobmandude9889.questBuilder.npc.NPCType;
import net.bobmandude9889.questBuilder.quest.TriggerData;
import net.bobmandude9889.questBuilder.quest.TriggerType;
import net.bobmandude9889.questBuilder.util.SkillType;
import net.bobmandude9889.questBuilder.util.Util;
import net.bobmandude9889.questBuilder.window.callback.TriggerEditCallback;

public class TriggerEditor extends JFrame {

	private static final long serialVersionUID = -3420151325993499911L;
	private JPanel contentPane;
	
	private JLabel lblAction;
	private JComboBox<String> comboBox;
	
	public static HashMap<TriggerType, HashMap<String, String>> triggers;
	
	TriggerData trigger;
	int index;
	TriggerEditCallback callback;
	
	static {
		triggers = new HashMap<>();
		
		triggers.put(enterRegion, Util.toHashMap("regionName*", "String"));
		triggers.put(leaveRegion, Util.toHashMap("regionName*", "String"));
		triggers.put(gainFlag, Util.toHashMap("flag*", "String"));
		triggers.put(loseFlag, Util.toHashMap("flag*", "String"));
		triggers.put(receiveItem, Util.toHashMap("type*", "Material", "name", "String"));
		triggers.put(sellItem, Util.toHashMap("type*", "Material"));
		triggers.put(buyItem, Util.toHashMap("type*", "Material"));
		triggers.put(reachLevel, Util.toHashMap("level*", "int", "skill*", "Skill"));
		triggers.put(reachXp, Util.toHashMap("xp*", "int", "skill*", "Skill"));
		triggers.put(giveNPCItem, Util.toHashMap("type*", "NPC", "item*", "Material", "name", "String"));
	}
	
	@SuppressWarnings("unchecked")
	public TriggerEditor(TriggerData trigger, int index, TriggerEditCallback callback) {
		List<HashMap<String, String>> cloneParams = new ArrayList<>();
		for (HashMap<String, String> map : trigger.params) {
			HashMap<String, String> cloneMap = new HashMap<>();
			cloneMap = (HashMap<String, String>) map.clone();
			cloneParams.add(cloneMap);
		}
		List<TriggerType> cloneTriggers = new ArrayList<>();
		for(TriggerType type : trigger.triggers) {
			cloneTriggers.add(type);
		}
		this.index = index;
		this.trigger = new TriggerData(cloneTriggers, cloneParams);
		this.callback = callback;
		
		setTitle("Trigger Editor");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 251, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		update();
	}
	
	public void update() {
		for(Component comp : contentPane.getComponents()) {
			contentPane.remove(comp);
		}
		
		lblAction = new JLabel("Action:");
		lblAction.setBounds(10, 11, 46, 14);
		contentPane.add(lblAction);
		
		comboBox = new JComboBox<>();
		comboBox.setBounds(10, 36, 100, 20);
		contentPane.add(comboBox);
		String[] types = new String[TriggerType.values().length];
		for (int i = 0; i < types.length; i++) {
			types[i] = TriggerType.values()[i].display();
		}
		comboBox.setModel(new DefaultComboBoxModel<String>(types));
		comboBox.setSelectedIndex(Arrays.asList(TriggerType.values()).indexOf(trigger.triggers.get(index)));
		comboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				trigger.triggers.set(index, TriggerType.values()[comboBox.getSelectedIndex()]);
				HashMap<String, String> map = new HashMap<>();
				for (String key : triggers.get(trigger.triggers.get(index)).keySet()) {
					String name = key.endsWith("*") ? key.substring(0,key.length() - 1) : key;
					String type = triggers.get(trigger.triggers.get(index)).get(key);
					if (type.equals("String")) {
						map.put(name, "");
					} else if (type.equals("Material")) {
						map.put(name, Material.ACACIA_DOOR.name());
					} else if (type.equals("int")) {
						map.put(name, "1");
					} else if (type.equals("NPC")) {
						map.put(name, NPCType.get(0).getName());
					} else if (type.equals("Skill")) {
						map.put(name, SkillType.ARCHERY.name());
					}
				}
				trigger.params.set(index, map);
				update();
			}
		});
		
		JButton btnSave = new JButton("Save");
		btnSave.setBounds(136, 7, 89, 23);
		contentPane.add(btnSave);
		btnSave.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				callback.onSave(trigger.triggers.get(index), trigger.params.get(index), index);
				TriggerEditor.this.setVisible(false);
				TriggerEditor.this.dispose();
			}
		});
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.setBounds(136, 35, 89, 23);
		contentPane.add(btnDelete);
		btnDelete.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				callback.onDelete(index);
				TriggerEditor.this.setVisible(false);
				TriggerEditor.this.dispose();
			}
		});
		
		HashMap<String, String> params = triggers.get(TriggerType.values()[comboBox.getSelectedIndex()]);
		int i = 0;
		for (String key : params.keySet()) {
			String name = key.endsWith("*") ? key.substring(0,key.length() - 1) : key;
			String type = params.get(key);
			
			JLabel lbl = new JLabel(name + ":");
			lbl.setBounds(10, 62 + (i * 46), 100, 14);
			contentPane.add(lbl);
			
			if (type.startsWith("String")) {
				JTextField field = new JTextField();
				field.setText(trigger.params.get(index).get(name));
				field.setBounds(10, 82 + (i * 46), 100, 20);
				contentPane.add(field);
				btnSave.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						trigger.params.get(index).put(name, field.getText());
					}
				});
			} else if (type.startsWith("Material")) {
				JComboBox<Material> comboBox1 = new JComboBox<>();
				comboBox1.setBounds(10, 82 + (i * 46), 100, 20);
				contentPane.add(comboBox1);
				Material defaultMaterial = Material.valueOf(trigger.params.get(index).get(name).toUpperCase());
				comboBox1.setModel(new ComboBoxModel<Material>() {
					
					Material selected = defaultMaterial;
					
					@Override
					public void removeListDataListener(ListDataListener l) {
					}
					
					@Override
					public int getSize() {
						return Material.values().length;
					}
					
					@Override
					public Material getElementAt(int index) {
						return Material.values()[index];
					}
					
					@Override
					public void addListDataListener(ListDataListener l) {
					}
					
					@Override
					public void setSelectedItem(Object selected) {
						this.selected = (Material) selected;
					}
					
					@Override
					public Object getSelectedItem() {
						return selected;
					}
				});
				btnSave.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						trigger.params.get(index).put(name, ((Material) comboBox1.getSelectedItem()).name());
					}
				});
			} else if (type.startsWith("int")) {
				JSpinner spinner = new JSpinner();
				spinner.setValue(Integer.parseInt(trigger.params.get(index).get(name)));
				spinner.setBounds(10, 82 + (i * 46), 100, 20);
				contentPane.add(spinner);
				btnSave.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						trigger.params.get(index).put(name, spinner.getValue().toString());
					}
				});
			} else if (type.startsWith("NPC")) {
				JComboBox<NPCType> comboBox1 = new JComboBox<>();
				comboBox1.setBounds(10, 82 + (i * 46), 100, 20);
				contentPane.add(comboBox1);
				NPCType defaultNPC = NPCType.get(trigger.params.get(index).get(name));
				comboBox1.setModel(new ComboBoxModel<NPCType>() {
					
					NPCType selected = defaultNPC;
					
					@Override
					public void removeListDataListener(ListDataListener l) {
					}
					
					@Override
					public int getSize() {
						return NPCType.types.size();
					}
					
					@Override
					public NPCType getElementAt(int index) {
						return NPCType.get(index);
					}
					
					@Override
					public void addListDataListener(ListDataListener l) {
					}
					
					@Override
					public void setSelectedItem(Object selected) {
						this.selected = (NPCType) selected;
					}
					
					@Override
					public Object getSelectedItem() {
						return selected;
					}
				});
				btnSave.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						trigger.params.get(index).put(name, ((NPCType) comboBox1.getSelectedItem()).getName());
					}
				});
			} else if (type.startsWith("Skill")) {
				JComboBox<SkillType> comboBox1 = new JComboBox<>();
				comboBox1.setBounds(10, 82 + (i * 46), 100, 20);
				contentPane.add(comboBox1);
				SkillType defaultSkill = SkillType.valueOf(trigger.params.get(index).get(name));
				comboBox1.setModel(new ComboBoxModel<SkillType>() {
					
					SkillType selected = defaultSkill;
					
					@Override
					public void removeListDataListener(ListDataListener l) {
					}
					
					@Override
					public int getSize() {
						return SkillType.values().length;
					}
					
					@Override
					public SkillType getElementAt(int index) {
						return SkillType.values()[index];
					}
					
					@Override
					public void addListDataListener(ListDataListener l) {
					}
					
					@Override
					public void setSelectedItem(Object selected) {
						this.selected = (SkillType) selected;
					}
					
					@Override
					public Object getSelectedItem() {
						return selected;
					}
				});
				btnSave.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						trigger.params.get(index).put(name, ((SkillType) comboBox1.getSelectedItem()).name());
					}
				});
			}
			i++;
		}
		
		trigger.triggers.set(index, TriggerType.values()[comboBox.getSelectedIndex()]);
		HashMap<String, String> map = new HashMap<>();
		for (String key : triggers.get(trigger.triggers.get(index)).keySet()) {
			String name = key.endsWith("*") ? key.substring(0,key.length() - 1) : key;
			String type = triggers.get(trigger.triggers.get(index)).get(key);
			if (type.equals("String")) {
				map.put(name, "");
			} else if (type.equals("Material")) {
				map.put(name, Material.ACACIA_DOOR.name());
			} else if (type.equals("int")) {
				map.put(name, "1");
			} else if (type.equals("NPC")) {
				map.put(name, NPCType.get(0).getName());
			} else if (type.equals("Skill")) {
				map.put(name, SkillType.ARCHERY.name());
			}
		}
		trigger.params.set(index, map);
		
		this.repaint();
	}
	
}
