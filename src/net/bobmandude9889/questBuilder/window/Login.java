package net.bobmandude9889.questBuilder.window;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import net.bobmandude9889.questBuilder.QuestBuilder;

public class Login extends JFrame {
	private static final long serialVersionUID = -6601933809119598252L;
	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField passwordField;

	private static final String USER_AGENT = "Mozilla/5.0";
	private static final String URL = "https://zerenthalrpg.com/sql_data.php";

	public Login() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 173, 182);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblZerenthalrpgUsername = new JLabel("ZerenthalRPG Username:");
		lblZerenthalrpgUsername.setBounds(10, 11, 148, 14);
		contentPane.add(lblZerenthalrpgUsername);
		
		textField = new JTextField();
		textField.setBounds(10, 36, 148, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setBounds(10, 67, 69, 14);
		contentPane.add(lblPassword);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(10, 92, 148, 20);
		contentPane.add(passwordField);
		
		JButton btnNewButton = new JButton("Login");
		btnNewButton.setBounds(10, 123, 148, 23);
		contentPane.add(btnNewButton);
		
		btnNewButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String password = new String(passwordField.getPassword());
				String username = textField.getText();
				
				try {
					URL url = new URL(URL);
					HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
					con.setRequestMethod("POST");
					con.setRequestProperty("User-Agent", USER_AGENT);
					
					con.setDoOutput(true);
					OutputStream os = con.getOutputStream();
					String params = "email=" + username + "&pass=" + password;
					os.write(params.getBytes());
					os.flush();
					os.close();
					
					int responseCode = con.getResponseCode();
					
					if (responseCode == HttpsURLConnection.HTTP_OK) {
						BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
						String inputLine;
						StringBuffer response = new StringBuffer();
						
						while ((inputLine = in.readLine()) != null) {
							response.append(inputLine);
						}
						in.close();
						
						String raw = response.toString();
						JSONParser parser = new JSONParser();
						JSONObject obj = (JSONObject) parser.parse(raw);
						if (obj.containsKey("error")) {
							JOptionPane.showMessageDialog(null, obj.get("error"), "Error", JOptionPane.ERROR_MESSAGE);
						} else {
							QuestBuilder.ip = (String) obj.get("ip");
							QuestBuilder.port = (String) obj.get("port");
							QuestBuilder.username = (String) obj.get("username");
							QuestBuilder.password = (String) obj.get("password");
							QuestBuilder.onLogin();
							Login.this.setVisible(false);
						}
					}
				} catch (IOException | ParseException e1) {
					e1.printStackTrace();
				}
			}
			
		});
	}
}
