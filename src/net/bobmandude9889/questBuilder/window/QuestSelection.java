package net.bobmandude9889.questBuilder.window;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;

import net.bobmandude9889.questBuilder.quest.Quest;

public class QuestSelection extends JFrame {

	private static final long serialVersionUID = 7351376954696111260L;
	private JPanel contentPane;

	public List<JPanel> panel;
	
	public JComboBox<String> quests;
	public JLabel lblId;
	public JLabel lblName;
	private JButton btnEdit;
	
	public static QuestSelection instance;
	
	public QuestSelection() {
		super("Quest Selection");
		instance = this;
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 200, 133);
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		quests = new JComboBox<String>();
		quests.setBounds(10, 11, 109, 20);
		contentPane.add(quests);
		
		lblId = new JLabel("ID: 1");
		lblId.setBounds(10, 73, 73, 20);
		contentPane.add(lblId);
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.setBounds(10, 42, 73, 23);
		contentPane.add(btnDelete);
		
		lblName = new JLabel("Name: Test Quest");
		lblName.setBounds(93, 72, 87, 20);
		contentPane.add(lblName);
		
		JButton btnCreate = new JButton("Create");
		btnCreate.setBounds(93, 42, 87, 23);
		contentPane.add(btnCreate);
		
		btnEdit = new JButton("Edit");
		btnEdit.setBounds(129, 10, 51, 23);
		contentPane.add(btnEdit);
		btnEdit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				QuestEditor editor = new QuestEditor(Quest.getQuests().get(quests.getSelectedIndex()));
				editor.setVisible(true);
			}
		});
		
		updateModel();
		
		quests.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int i = quests.getSelectedIndex();
				Quest quest = Quest.getQuests().get(i);
				lblId.setText("ID: " + quest.id);
				lblName.setText("Name: " + quest.name);
			}
		});
		
		btnDelete.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int j = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete this quest?", "Delete", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
				
				if (j == 0) {
					int i = quests.getSelectedIndex();
					Quest.getQuests().get(i).delete();
					updateModel();
				}
			}
		});
		
		btnCreate.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Quest.createQuest();
				updateModel();
				quests.setSelectedIndex(quests.getItemCount() - 1);
			}
		});
	}
	
	public void updateModel() {
		List<Quest> questObjects = Quest.getQuests();
		String[] questNames = new String[questObjects.size()];
		
		for (int i = 0; i < questNames.length; i++) {
			questNames[i] = questObjects.get(i).name;
		}
		
		quests.setModel(new DefaultComboBoxModel<String>(questNames));
		
		Quest quest = Quest.getQuests().get(0);
		lblId.setText("ID: " + quest.id);
		lblName.setText("Name: " + quest.name);
	}
}
