package net.bobmandude9889.questBuilder.window;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import net.bobmandude9889.questBuilder.window.callback.CommandEditCallback;

public class CommandEditor extends JFrame {
	
	private static final long serialVersionUID = 7765376615096994579L;
	private JPanel contentPane;
	private JTextField textField;

	public CommandEditor(String command, CommandEditCallback callback) {
		setTitle("Command Editor");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 336, 141);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		JLabel lblCommand = new JLabel("Command:");
		lblCommand.setBounds(10, 11, 102, 14);
		contentPane.add(lblCommand);
		
		JButton btnSave = new JButton("Save");
		
		textField = new JTextField();
		textField.setBounds(10, 36, 300, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		textField.setText(command);
		textField.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
					btnSave.doClick();
			}
			
		});
		
		btnSave.setBounds(63, 67, 89, 23);
		contentPane.add(btnSave);
		btnSave.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				callback.onSave(textField.getText());
				CommandEditor.this.setVisible(false);
				CommandEditor.this.dispose();
			}
			
		});
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.setBounds(162, 67, 89, 23);
		contentPane.add(btnDelete);
		btnDelete.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				callback.onDelete();
				CommandEditor.this.setVisible(false);
				CommandEditor.this.dispose();
			}
			
		});
	}
}
