package net.bobmandude9889.questBuilder.window;

import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListDataListener;

import net.bobmandude9889.questBuilder.npc.NPCType;

public class NPCEditor extends JFrame {

	private static final long serialVersionUID = -4091523684871255395L;
	private JPanel contentPane;
	private JTextField txtBanker;
	
	public NPCEditor() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 272, 168);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblName = new JLabel("Name:");
		lblName.setBounds(10, 11, 84, 14);
		contentPane.add(lblName);
		
		txtBanker = new JTextField();
		txtBanker.setText("BANKER");
		txtBanker.setBounds(10, 36, 100, 20);
		contentPane.add(txtBanker);
		txtBanker.setColumns(10);
		
		JButton btnSave = new JButton("Save");
		btnSave.setBounds(10, 67, 100, 23);
		contentPane.add(btnSave);
		
		JButton btnDeleteButton = new JButton("Delete");
		btnDeleteButton.setBounds(10, 101, 100, 23);
		contentPane.add(btnDeleteButton);
		
		JLabel lblAction = new JLabel("Action:");
		lblAction.setBounds(149, 11, 68, 14);
		contentPane.add(lblAction);
		
		JComboBox<String> comboBox = new JComboBox<String>();
		comboBox.setBounds(149, 36, 100, 20);
		contentPane.add(comboBox);
		comboBox.setModel(new ComboBoxModel<String>() {

			@Override
			public int getSize() {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public String getElementAt(int index) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void addListDataListener(ListDataListener l) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void removeListDataListener(ListDataListener l) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setSelectedItem(Object anItem) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public Object getSelectedItem() {
				// TODO Auto-generated method stub
				return null;
			}
		});
	}
}
