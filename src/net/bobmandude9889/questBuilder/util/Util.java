package net.bobmandude9889.questBuilder.util;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import net.bobmandude9889.questBuilder.dialogue.DialogueAction;
import net.bobmandude9889.questBuilder.window.CommandEditor;
import net.bobmandude9889.questBuilder.window.callback.CommandEditCallback;

public class Util {
	
	public static String fixCaps(String text) {
		String result = "";
		for (String word : text.split("_")) {
			result += word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase() + " ";
		}
		return result.substring(0, result.length() - 1);
	}
	
	public static HashMap<String, String> toHashMap(String... strings) {
		HashMap<String, String> map = new HashMap<>();
		for (int i = 0; i < strings.length; i+=2) {
			map.put(strings[i], strings[i+1]);
		}
		return map;
	}
	
	public static void setupAction(DialogueAction action, int xOff, Container contentPane, JButton saveBtn) {
		for (int i = 0; i < action.getClass().getDeclaredFields().length; i++) {
			Field field = action.getClass().getDeclaredFields()[i];
			field.setAccessible(true);
			if (!field.getName().equals("commands")) {
				JLabel title = new JLabel(Util.fixCaps(field.getName()));
				title.setBounds(146 + xOff, 45 + (i * 25), 83, 14);
				contentPane.add(title);

				JCheckBox enable = new JCheckBox("");
				enable.setBounds(236 + xOff, 41 + (i * 25), 21, 23);
				contentPane.add(enable);
				
				if (field.getType().equals(int.class)) {
					JSpinner spinner = new JSpinner();
					saveBtn.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							action.set(field.getName(),(int) spinner.getValue());
						}
					});
					spinner.addChangeListener(new ChangeListener() {

						@Override
						public void stateChanged(ChangeEvent e) {
							if ((int) spinner.getValue() == -1) {
								enable.setSelected(false);
							} else {
								enable.setSelected(true);
							}
						}
						
					});
					spinner.setBounds(268 + xOff, 43 + (i * 25), 86, 20);
					contentPane.add(spinner);
					try {
						int value = field.getInt(action);
						spinner.setValue(value);
						if (value != -1) {
							enable.setSelected(true);
						}
					} catch (IllegalArgumentException | IllegalAccessException e) {
						e.printStackTrace();
					}
				} else if (field.getType().equals(String.class)) { 
					JTextField textField = new JTextField();
					saveBtn.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							action.set(field.getName(),textField.getText());
						}
					});
					textField.addKeyListener(new KeyListener() {
						
						@Override
						public void keyTyped(KeyEvent e) {
						}
						
						@Override
						public void keyReleased(KeyEvent e) {
							if (textField.getText().length() > 0) {
								enable.setSelected(true);
							} else {
								enable.setSelected(false);
							}
						}
						
						@Override
						public void keyPressed(KeyEvent e) {
						}
					});
					textField.setBounds(268 + xOff, 43 + (i * 25), 86, 20);
					contentPane.add(textField);
					textField.setColumns(10);
					try {
						String value = (String) field.get(action);
						textField.setText(value);
						if (value != null) {
							enable.setSelected(true);
						}
					} catch (IllegalArgumentException | IllegalAccessException e) {
						e.printStackTrace();
					}
				}
			}
		}
		
		JLabel commandsLbl = new JLabel("Commands");
		commandsLbl.setBounds(146 + xOff, 300, 83, 14);
		contentPane.add(commandsLbl);

		JCheckBox enable = new JCheckBox("");
		enable.setBounds(236 + xOff, 296, 21, 23);
		contentPane.add(enable);
		if (action.commands == null || action.commands.size() == 0) {
			enable.setSelected(false);
		} else {
			enable.setSelected(true);
		}
		
		JComboBox<String> comboBox = new JComboBox<>();
		comboBox.setBounds(268 + xOff, 296, 86, 20);
		contentPane.add(comboBox);
		updateModel(action, comboBox);
		comboBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (comboBox.getSelectedIndex() == comboBox.getItemCount() - 1) {
					CommandEditor editor = new CommandEditor("", new CommandEditCallback() {
						
						@Override
						public void onSave(String command) {
							if (action.commands == null)
								action.commands = new ArrayList<String>();
							action.commands.add(command);
							updateModel(action, comboBox);
							enable.setSelected(true);
						}
	
						@Override
						public void onDelete() {
						}
						
					});
					editor.setVisible(true);
				} else {
					CommandEditor editor = new CommandEditor(comboBox.getItemAt(comboBox.getSelectedIndex()), new CommandEditCallback() {
	
						@Override
						public void onSave(String command) {
							if (action.commands == null)
								action.commands = new ArrayList<String>();
							action.commands.set(comboBox.getSelectedIndex(), command);
							updateModel(action, comboBox);
						}
	
						@Override
						public void onDelete() {
							if (action.commands == null) {
								action.commands = new ArrayList<String>();
								return;
							}
							action.commands.remove(comboBox.getSelectedIndex());
							updateModel(action, comboBox);
							if (action.commands.size() == 0)
								enable.setSelected(false);
						}
						
					});
					editor.setVisible(true);
				}
			}
			
		});
	}

	public static void updateModel(DialogueAction action, JComboBox<String> comboBox) {
		String[] commands = {"Add Command"};
		
		if (action.commands != null) {
			commands = new String[action.commands.size() + 1];
			for (int i = 0; i < commands.length - 1; i++) {
				commands[i] = action.commands.get(i);
			}
			commands[commands.length - 1] = "Add Command";
		}
		comboBox.setModel(new DefaultComboBoxModel<String>(commands));
	}
	
}
