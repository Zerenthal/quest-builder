package net.bobmandude9889.questBuilder.util.tellraw;

import org.json.simple.JSONObject;

public class TellrawBookText extends TellrawChatText {

	public TellrawBookText(String text) {
		super(text);
	}

	@SuppressWarnings("unchecked")
	public TellrawBookText setGotoPageOnClick(int page) {
		JSONObject clickObj = new JSONObject();
		clickObj.put("action", "change_page");
		clickObj.put("value", page);
		obj.put("clickEvent", clickObj);
		return this;
	}
	
}
