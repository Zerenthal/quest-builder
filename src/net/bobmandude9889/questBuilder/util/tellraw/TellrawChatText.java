package net.bobmandude9889.questBuilder.util.tellraw;

import org.bukkit.ChatColor;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

@SuppressWarnings("unchecked")
public class TellrawChatText extends TellrawText {

	public TellrawChatText(String text) {
		super(text);
	}
	
	@Override
	public TellrawChatText setText(String text) {
		return (TellrawChatText) super.setText(text);
	}
	
	@Override
	public TellrawChatText setColor(ChatColor color) {
		return (TellrawChatText) super.setColor(color);
	}
	
	public TellrawChatText setRunCommandOnClick(String command) {
		JSONObject clickObj = new JSONObject();
		clickObj.put("action", "run_command");
		clickObj.put("value", command);
		obj.put("clickEvent", clickObj);
		return this;
	}

	public TellrawChatText setSuggestCommandOnClick(String command) {
		JSONObject clickObj = new JSONObject();
		clickObj.put("action", "suggest_command");
		clickObj.put("value", command);
		obj.put("clickEvent", clickObj);
		return this;
	}
	
	public TellrawChatText setOpenURLOnClick(String url) {
		JSONObject clickObj = new JSONObject();
		clickObj.put("action", "open_url");
		clickObj.put("value", url);
		obj.put("clickEvent", clickObj);
		return this;
	}

	public TellrawChatText setShowTextOnHover(TellrawText text) {
		setShowTextOnHover(new TellrawText[] {text});
		return this;
	}
	
	public TellrawChatText setShowTextOnHover(TellrawText[] texts) {
		JSONObject hoverObj = new JSONObject();
		hoverObj.put("action", "show_text");
		
		JSONArray textObjs = new JSONArray();
		
		for(int i = 0; i < texts.length; i++) {
			textObjs.add(i, texts[i].obj);
		}
		
		JSONObject hoverText = new JSONObject();
		hoverText.put("text", "");
		hoverText.put("extra", textObjs);
		
		hoverObj.put("value", hoverText);
		
		obj.put("hoverEvent", hoverObj);
		return this;
	}
	
}
