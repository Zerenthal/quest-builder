package net.bobmandude9889.questBuilder.util;

public enum SkillType {

	MELEE("Melee"),
	ARCHERY("Archery"),
	SMITHING("Smithing"),
	MINING("Mining"),
	WOODCUTTING("Woodcutting"),
	FARMING("Farming"),
	FISHING("Fishing"),
	COOKING("Cooking");
	
	public String name;
	
	private SkillType(String name){
		this.name = name;	
	}
	
	@Override
	public String toString() {
		return name;
	}
	
}
