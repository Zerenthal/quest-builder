package net.bobmandude9889.questBuilder.util.sql;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class SetInfo {
	public List<Map<String, Object>> resultMap;

	public SetInfo(ResultSet result) {
		resultMap = resultToMap(result);
	}


	private List<Map<String, Object>> resultToMap(ResultSet set) {
		List<Map<String, Object>> row = new ArrayList<Map<String, Object>>();
		ResultSetMetaData metaData;
		try {
			metaData = set.getMetaData();
			int colCount = metaData.getColumnCount();

			while (set.next()) {
				Map<String, Object> columns = new HashMap<String, Object>();
				for (int i = 1; i <= colCount; i++) {
					columns.put(metaData.getColumnLabel(i),
							set.getObject(i));
				}

				row.add(columns);
			}
			set.beforeFirst();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return row;
	}

	public int getSize() {
		return resultMap.size();
	}
	
	public Object getValue(int row, String col){
		return resultMap.get(row).get(col);
	}
}
