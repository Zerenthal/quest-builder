package net.bobmandude9889.questBuilder.util.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SqlStatement {
	private Connection conn;

	public SqlStatement(DBConn conn) {
		this.conn = conn.getConn();
	}
	
	public void insert(String table, String[] cols, Object[] vals) {
		String que = "INSERT INTO " + table + " (";
		String colStmt = "";
		for (String col : cols){
			colStmt += ", " + col;
		}
		colStmt = colStmt.replaceFirst(", ", "");
		que += colStmt + ") VALUES (";
		String valStmt = "";
		for (Object val : vals){
			valStmt += ", ";
			if (val instanceof Integer){
				valStmt += String.valueOf(val);
			} else {
				valStmt += "'" + String.valueOf(val) + "'";
			}
		}
		valStmt = valStmt.replaceFirst(", ", "");
		que += valStmt + ");";
		execute(que);
	}
	public ResultSet select(String table, String condition){
		String[] selection = {"*"};
		return select(table, condition, selection);

	}
	
	public ResultSet select(String table){
		String[] selection = {"*"};
		return select(table, "1", selection);

	}
	
	public ResultSet select(String table, String... selection){
		return select(table, "1", selection);
	}
	
	public ResultSet select(String table, String condition, String... selection){
		String selList = "";
		for (String sel : selection){
			selList += ", " + sel;	
		}
		selList = selList.replaceFirst(", ", "");
		String q = "SELECT " + selList + " FROM " + table + " WHERE " + condition;
		return query(q);
	}
	
	public void execute(String command) {
		Statement stmt = null;
		try {
			stmt = conn.createStatement();
			stmt.execute(command);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public ResultSet query(String command) {
		
		Statement stmt = null;
		ResultSet result = null;
		try {
			stmt = conn.createStatement();
			result = stmt.executeQuery(command);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public ResultSet prepQue(String command){
		return query(command);
	}
	
	public ResultSet prepQue (String command, String types, String[] inputs){
		return prepQue(command, types, (Object)inputs);
	}

	public ResultSet prepQue(String command, String types, int[] inputs){
		return prepQue(command, types, (Object)inputs);
	}
	
	public ResultSet prepQue(String command, String types, Object... inputs) {
		PreparedStatement stmt = null;
		ResultSet result = null;
		try {
			stmt = conn.prepareStatement(command);
			int count = 1;
			for (Character type : types.toCharArray()) {
				if (type.equals('i')) {
					if (inputs[count - 1] instanceof Integer) {
						stmt.setInt(count, (Integer) inputs[count - 1]);
					} else 
						stmt.setInt(count, Integer.parseInt((String) inputs[count - 1]));
				} else {
					stmt.setString(count, String.valueOf(inputs[count - 1]));
				}
				count++;
			}

			result = stmt.executeQuery();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public void prepExe(String command, String types, Object... inputs) {
		PreparedStatement stmt = null;
		try {
			stmt = conn.prepareStatement(command);
			int count = 1;
			for (Character type : types.toCharArray()) {
				if (type.equals('i')) {
					if (inputs[count - 1] instanceof Integer) {
						stmt.setInt(count, (Integer) inputs[count - 1]);
					} else 
						stmt.setInt(count, Integer.parseInt((String) inputs[count - 1]));
				} else {
					stmt.setString(count, String.valueOf(inputs[count - 1]));
				}
				count++;
			}

			stmt.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
}
