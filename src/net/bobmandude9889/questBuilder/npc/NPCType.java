package net.bobmandude9889.questBuilder.npc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import net.bobmandude9889.questBuilder.QuestBuilder;
import net.bobmandude9889.questBuilder.util.sql.DBConn;
import net.bobmandude9889.questBuilder.util.sql.SqlStatement;

public class NPCType {

	public static HashMap<String, NPCType> types;

	static {
		types = new HashMap<>();

		DBConn conn = QuestBuilder.sqlMCConn;

		SqlStatement stmt = new SqlStatement(conn);
		ResultSet results = stmt.select("NPCTypes");

		try {
			while (results.next()) {

				JSONObject action = (JSONObject) new JSONParser().parse(results.getString("Action"));
				
				HashMap<String, String> methods = new HashMap<>();
				String defaultMethod = null;
				
				for(Object key : action.keySet()) {
					methods.put((String) key, (String) action.get(key));
				}

				NPCType npc = new NPCType(methods,defaultMethod);
				types.put(results.getString("Name"), npc);
				
			}
		} catch (SQLException | ParseException e) {
			e.printStackTrace();
		}
	}

	public static NPCType get(String name) {
		return types.get(name);
	}
	
	public static NPCType get(int i) {
		int index = 0;
		for (NPCType type : types.values()) {
			if (i == index)
				return type;
			index++;
		}
		return null;
	}

	public HashMap<String, String> methods;
	public String defaultMethod;

	private NPCType(HashMap<String, String> methods, String defaultMethod) {
		this.methods = methods;
		this.defaultMethod = defaultMethod;
	}
	
	public String getName() {
		for (String key : types.keySet()) {
			if (types.get(key) == this) {
				return key;
			}
		}
		return null;
	}
	
	@Override
	public String toString() {
		return getName();
	}
}
