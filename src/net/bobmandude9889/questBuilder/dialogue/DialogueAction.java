package net.bobmandude9889.questBuilder.dialogue;

import java.lang.reflect.Field;
import java.util.List;

import org.json.simple.JSONObject;

public class DialogueAction {
	
	int open_menu = -1;
	String message;
	String title;
	String subtitle;
	int title_fade_in = -1;
	int title_fade_out = -1;
	int title_length = -1;
	String flag;
	String remove_flag;
	int start_quest = -1;
	public List<String> commands;
	
	public void set(String key, Object value) {
		try {
			Field field = DialogueAction.class.getDeclaredField(key);
			field.setAccessible(true);
			field.set(this, value);
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public String toJSON() {
		JSONObject object = new JSONObject();
		for (Field field : DialogueAction.class.getDeclaredFields()) {
			try {
				if (field.get(this) != null && (field.getType() != int.class || field.getInt(this) >= 0) && (field.getType() != String.class || !field.get(this).equals("")))
					object.put(field.getName(), field.get(this));
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return object.toJSONString();
	}
	
}
