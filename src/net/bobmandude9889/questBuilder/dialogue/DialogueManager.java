package net.bobmandude9889.questBuilder.dialogue;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import net.bobmandude9889.questBuilder.QuestBuilder;
import net.bobmandude9889.questBuilder.util.sql.DBConn;
import net.bobmandude9889.questBuilder.util.sql.SqlStatement;
import net.bobmandude9889.questBuilder.util.tellraw.TellrawBuilder;
import net.bobmandude9889.questBuilder.util.tellraw.TellrawChatText;

public class DialogueManager {

	static HashMap<Integer, String> dialogueMap;
	static HashMap<String, DialogueAction> actionMap;
	static String[] actionOptions = {"open_menu","commands","message","title","subtitle","title_fade_in","title_fade_out","title_length","flag","remove_flag","start_quest"};
	
	public static void loadConfig() {
		dialogueMap = new HashMap<>();
		actionMap = new HashMap<>();
		
		DBConn conn = QuestBuilder.sqlMCConn;
		
		SqlStatement stmt = new SqlStatement(conn);
		ResultSet results = stmt.select("Dialogue");
		
		try {
			while(results.next()) {
				TellrawBuilder b = new TellrawBuilder().addText(results.getString("Text"));
				
				ResultSet options = stmt.select("DialogueOptions","ParentDialogue=" + results.getInt("Id") + " ORDER BY Id");
				
				while(options.next()) {
					b.addText("\n\n");
					TellrawChatText text = new TellrawChatText(ChatColor.translateAlternateColorCodes('&', options.getString("Text")));
					
					JSONObject actionObj = (JSONObject) new JSONParser().parse(options.getString("Action"));
					
					if(actionObj.size() > 1) {
						String actionId = UUID.randomUUID().toString();
						DialogueAction action = new DialogueAction();
						for(String act : actionOptions) {
							if(actionObj.containsKey(act)) {
								action.set(act, actionObj.get(act));
							}
						}
						actionMap.put(actionId, action);
					}
					b.addText(text);
				}
				dialogueMap.put(results.getInt("Id"), b.getJSON());
			}
		} catch (SQLException | ParseException e) {
			e.printStackTrace();
		}
	}

}
