package net.bobmandude9889.questBuilder;

import java.awt.EventQueue;

import net.bobmandude9889.questBuilder.util.sql.DBConn;
import net.bobmandude9889.questBuilder.window.Login;
import net.bobmandude9889.questBuilder.window.QuestSelection;

public class QuestBuilder {
	
	public static DBConn sqlMCConn; 

	public static String ip;
	public static String port;
	public static String username;
	public static String password;
	
	public static void main(String[] args) {
		Login login = new Login();
		login.setVisible(true);
	}
	
	public static void onLogin() {
		sqlMCConn = new DBConn(ip, port, "zerentha_minecraft", username, password);
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					QuestSelection frame = new QuestSelection();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
